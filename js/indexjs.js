//Grab all the sections to set their height and width appropriately.


//Set section height and width when the document is loaded in the browser
$(document).ready(function() {
	var windowHeightReady = $(window).height();
	var windowWidthReady = $(window).width();
	
	var helloSection = $("#hello");
	var skillsSection = $("#skills");
	var courseworkSection = $("#coursework");
	var connectSection = $("#connect");

	helloSection.css("height", windowHeightReady);
	helloSection.css("background-image", "url(images/background_fullsize.png)");
	helloSection.css("background-size", "cover");

	skillsSection.css("height", windowHeightReady);
	skillsSection.css("background-color", "black");

	courseworkSection.css("height", windowHeightReady);
	courseworkSection.css("background-color", "black");

	connectSection.css("height", windowHeightReady);
	connectSection.css("background-color", "black");
});

//Set setion height and width when the window is resized by the user on the desktop or tablet
$(window).resize(function() {
	var windowHeightResize = $(window).height();
	var windowwidthResize = $(window).width();
	
	helloSection.css("height", windowHeightResize);
	
	contactSection.css("height", windowHeightResize);
});